package modelo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;





/**
 *
 * @author JonathanRubio 1151521 Ingenieria de sistemas
 */





public class txt {

    private static final String direccion = "C:\\Users\\usuario\\Desktop\\netbeans_seed-master\\ProyectoSEED\\txt";

    /**
     * Metodo que crea el archivo txt Toma la direccion de memoria de mi pc y la
     * crea el txt
     */
    public String crearArchivo(String nombreArchivo) {

        nombreArchivo += ".txt";
        File archivo;

        try {

            archivo = new File(direccion + "\\" + nombreArchivo);
            if (archivo.createNewFile()) {
                return "Archivo subido con exito";
            } else {
                return "no se creo nada";
            }
        } catch (Exception e) {
            return "error al subir archivo" + "\n"
                    + e;
        }
    }

    /**
     * toma el nombre del archivo txt que si se creo anteriormente deberia estar
     * guardado en una carpeta txt del proyecto solo toma el nombre del archivo
     * y escribe en el
     */
    public String escribir(String nombre, String texto) {

        String x = nombre;
        nombre = direccion + "\\" + x + ".txt";
        File archivo;
        FileWriter fw;
        BufferedWriter bw;
        PrintWriter pw;

        try {

            archivo = new File(nombre);
            fw = new FileWriter(archivo);
            bw = new BufferedWriter(fw);
            pw = new PrintWriter(bw);

            pw.write(texto);

            if (archivo.exists()) {

            }

            bw.close();
            pw.close();

            return "Se escribio correctamente en el archivo";

        } catch (Exception e) {

            return "no se pudo escribir correctamente en el archivo" + "\n"
                    + e;

        }
    }

    /**
     * abre el txt con el nobmre de la direccion donde esta guardado en el
     * proyecto y lo muestro de la tal manera que se pueda apreciar el documento
     */
    public String leer(String nombreArchivo) {

        String x = nombreArchivo;
        nombreArchivo = direccion + "\\" + x + ".txt";
        String mensaje = "";
        File archivo;
        FileReader fr;
        BufferedReader br;

        try {

            archivo = new File(nombreArchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null) {
                mensaje += linea + "\n";
            }

            br.close();
            fr.close();

            return mensaje;

        } catch (Exception e) {

            return "Error, No se pudo leer" + "\n"
                    + e;

        }

    }

}
