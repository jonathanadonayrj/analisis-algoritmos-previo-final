package modelo;
import java.util.Iterator;
import sun.net.www.content.audio.x_aiff;
import ufps.util.colecciones_seed.*;





/**
 *
 * @author JonathanRubio 
 * 1151521 
 * Ingenieria de sistemas
 */





public class arbolDigital {

    
    //variable utilizada para imprimir el arbol binario
    ArbolBinario<String> x;
    //variable utilizada para guardar y no perder la raiz del arbol binario
    NodoBin<String> raiz=new NodoBin<>();
    
    
     
        
    /**
     * decoracion que llama el metodo crear arbol frase
     * el cual lleva todo el codigo y es privado
     */
    
    public ArbolBinario ArbolFrase(String[] frase){
    
        ArbolBinario x= crearArbolFrase(frase);
        return x;
    }
    
    /**
     * decoracion que llama el metodo crear arbol palabra
     * el cual lleva todo el codigo y es privado
     */
    
    
    public ArbolBinario ArbolPalabra(String[] palabra){
       
       ArbolBinario x= crearArbolPalabra(palabra);
       return x;
       
    }
    
    
    /**
     * toma una letra 
     * y lo vuelve binario
     */
    
    public String binLetra(String palabra) {
        
        return codLetra(palabra);
        
    }  
    
    
    /**
     metodo split para 
     * la frase del texto
     */
    public String[] frase(String frase){
    
        return frase.split(" ");
    
    }
   
    
    /**
     no funciona :v
     */
    
    public String arbolTxt(String text) {

        String[] x = text.split(" ");
        String[] palabraBin;
        String[] binario;
        
        for (int i = 0; i < x.length; i++) {
        
            //palabraBin= binPalabra(x[i]).split("-");
            //binario= palabraBin[i].split("");
            
            //for (int j = 0; j < binario.length; j++) {
                            
            //}
            
        }
      return "";  
    }
    
    /**
     * necesita del metodo crear arbol palabra  
     * concatena las palabras 
     * para forma un arbol completo con toda
     * la frase ingresada
     * concatena al lado derecho cada vez que se encuentra
     * una nueva palabra
     */
    
    public ArbolBinario crearArbolFrase(String[] frase){
    
        ArbolBinario arbol= new ArbolBinario();
       
        for (int i = 0; i < frase.length; i++) {
            String[] palabra= frase[i].split("");
            ArbolBinario aux= ArbolPalabra(palabra);
            arbol.insertarHijoDer(this.raiz, aux.getRaiz());
        }
    return arbol;    
    }
    
    
    
    /**
    toma una palabra
    * la parte en un vector
    * por sus letras
    * concatena al lado izquierdo
     */
    
    private ArbolBinario crearArbolPalabra(String[] palabra){
       
        String bin="";
        ArbolBinario arbol= new ArbolBinario();
        NodoBin nuevo=new NodoBin();
        NodoBin aux= new NodoBin();
        
        for (int i = 0; i < palabra.length; i++) {
            
            bin= binLetra(palabra[i]);
            nuevo.setInfo(bin);
            if(raiz==null){
                raiz=nuevo;
                arbol.setRaiz(nuevo);
                aux=raiz;
            }else{
                  arbol.insertarHijoIzq(aux, nuevo);        
                  aux=  nuevo;
            }
         }
        
        
        return arbol;
    }

    //public String binPalabra(String palabra) {
       
      //  String[] divisionPalabra= palabra.split("");
        //String rta="";   
         //for (int i = 0; i < divisionPalabra.length; i++) {
           // rta+= binLetra(divisionPalabra[i])+"-";
        //}
        //return rta;
    //}
    
  
/**
 metodo switch 
 * que se encarga 
 * de pasar una letra a binario
 */
    private String codLetra(String palabra) {

        switch (palabra) {

            case "a":
                return "00001";
            case "b":
                return "00010";
            case "c":
                return "00011";
            case "d":
                return "00100";
            case "e":
                return "00101";
            case "f":
                return "00110";
            case "g":
                return "00111";
            case "h":
                return "01000";
            case "i":
                return "01001";
            case "j":
                return "01010";
            case "k":
                return "01011";
            case "l":
                return "01100";
            case "m":
                return "01101";
            case "n":
                return "01110";
            case "o":
                return "01111";
            case "p":
                return "10000";
            case "q":
                return "10001";
            case "r":
                return "10010";
            case "s":
                return "10011";
            case "t":
                return "10100";
            case "u":
                return "10101";
            case "v":
                return "10110";
            case "w":
                return "10111";
            case "x":
                return "11000";
            case "y":
                return "11001";
            case ":":
                return "11010";
            case ";":
                return "11011";
            case ",":
                return "11100";
            case "-":
                return "11101";
        }
        return "";
    }

}
