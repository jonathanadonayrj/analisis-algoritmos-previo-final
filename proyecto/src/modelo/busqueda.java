package modelo;





/**
 *
 * @author JonathanRubio 
 * 1151521 
 * Ingenieria de sistemas
 */





public class busqueda {

    
    //variable ocupada en un metodo para  la aparicion de la palabra
    int apariciones = 0;
    //variable ocupada en un metodo para la aparacion de la palabra en una linea
    String linea = "";

    
    /**
     retorna la posicion x y 
     * de la palabra ingresada
     * necesita ademas de ello
     * el texto ya pasado en String
     */
    
    public String posicionXY(String palabra, String text) {

        String[] aux1 = palabra.split("\n");
        String rtas = "";

        for (int i = 0; i < aux1.length; i++) {

            String[] tx2 = aux1[i].split(" ");
            for (int j = 0; j < tx2.length; j++) {

                if (tx2[j].equals(text)) {
                    rtas += "encontrada:  (en x) " + i + " y (en y) : " + j + "\n";

                }

            }
        }
        return rtas;
    }

    
    /**
     llena una matriz la cual
     * tiene todas las palabras 
     * escritas 
     * con el fin de recorrerla 
     * y ver la cantidad de veces que aparece la palabra
     */
    private String[][] llenarMatriz(String text) {

        String[] aux = text.split(" ");
        String[][] rta = new String[maxX(text)][maxY(text)];

        int x = 0;
        int y = 0;

        for (int i = 0; i < aux.length; i++) {

            if (aux[i] == "-") {
                y++;
                x = 0;
            }
            rta[x][y] = aux[i];
            x++;
        }

        return rta;
    }

    /**
     * metodo que busca la cantidad mayor de cada linea para crear la pos x de
     * la matriz resultante
     */
    private int maxX(String text) {
        int x = 0;
        int aux = 0;
        String mensaje = "";
        String[] tx = text.split("\n");
        for (int i = 0; i < tx.length; i++) {

            String[] tx2 = tx[i].split(" ");
            for (int j = 0; j < tx2.length; j++) {
                aux++;
            }
            if (x < aux) {
                x = aux;
            }
            aux = 0;
        }
        return x;
    }

    /**
     * metodo que busca la cantidad mayor de cada linea para crear la pos y de
     * la matriz resultante
     */
    private int maxY(String text) {
        int y = 0;
        String[] tx = text.split("\n");
        for (int i = 0; i < tx.length; i++) {
            y++;
        }
        return y;
    }

    /**
     * muestra la linea
     * en la cual esta
     * la palabra, o las lineas
     */
    
    private String linea() {
        return "Linea en la que aparece:  " + this.linea;
    }

    /**
     * muestra el completo
     * o la cantidad
     * completa de apariciones 
     * de la palabra
     */
    
    private String cantApariciones() {
        return "La cantidad de apariciones fueron:  " + this.apariciones;
    }
}
